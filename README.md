# mpy

python3 extension for `matrop` in Molpro

## Getting started

Requirements: python3 `numpy` and `lxml`.

## Usage

```
cd <working dir>
ln -s <path_to_mpy>/mp.py .
cp <path_to_mpy>/{exportdata,importdata} .
```
Modify the `exportdata` and `importdata` according to your needs and include into the Molpro input file, e.g,
```
...
include,exportdata
system,'test.py',' > test.log'

include,importdata
...
```
See `test.mol` as an example. (Feel free to combine `exportdata`, python-call and `importdata` 
into a single file.) Note that `include` in Molpro is evaluated at the beginning of the calculation, therefore it cannot be used to set Molpro variables by the python script. Instead, Molpro variables (like `core`, `charge` etc.) can be set using `readvar` keyword in Molpro, i.e., if file `pyvars` contains definitions of variables one can include it after the `system` call as

```
readvar,pyvars
```

Create a python script (called `test.py` in the example from above), import `mp`, and set up the data, e.g.,
```
#!/usr/bin/env python3
from mp import *

setup_mpinfo(exportdata_file="exportdata")
```

`setup_mpinfo(exportdata_file="exportdata")` initializes a structure `MP` which holds various information 
about the molecule and the reference determinant. Currently:
* `MP.norbs4irreps` number of orbitals in each irrep
* `MP.molpro_files` map of variables defined in `exportdata`
* `MP.occvec` occupation vector (ones for occupied orbitals and zeros for empty 
orbitals)
* `MP.closvec` closed-shell vector (ones for closed-shell orbitals and zeros 
otherwise)
* `MP.occupation` true occupation vector from molpro 
(can be non-integer for natural orbitals)
* `MP.ncore` number of core orbitals in each symmetry
* `MP.charge` charge of the molecular system
  
The main structure is called `STen` (symmetric tensor).
It can be initialized with `MP.norbs4irreps` (default). 
This will allocate the space without setting the numbers.
By default, matrices will be created. In order to create vectors you can 
specify the `ndim` parameter, e.g.,
```
Vec = STen(ndim=1)
```
Then the numbers can be either read in (see below) or one can set them 
to 0, 1 or identity using `<ten>.zeros()`, `<ten>.ones`, or `<ten>.identity()`,
respectively, e.g., to create an identity matrix use 
```
One = STen().identity()
```
The import from Molpro can be done at the allocation step, e.g., to import orbitals, 
```
C = STen('ORBFILE')
```
The name `'ORBFILE'` has to be consistent with the variable name in `exportdata`. I suggest naming it according to the `matrop` convention +`'FILE'` (i.e., `'ORBFILE'` for orbitals, `'SFILE'` for the overlap, etc.).
One can also import from an ASCII file directly using `<ten>.import_from_file(<filename>)`.

Export to an ASCII file works similarly: use `<ten>.export('<variable name from exportdata>'[,num_in_line])` or `<ten>.export_to_file('filename'[,num_in_line])` to export to a file defined in `exportdata` or to a file `'filename'`, respectively. Parameter `num_in_line` is an optional integer defining how many numbers will be printed per line (default: 5).

The operations are similar to numpy, i.e., 
* `+` and `-` addition and subtraction 
* `*` elementwise multiplication 
* `@` (or `matmul` for python3.4) matrix multiplication
* `-<ten>` negative of a tensor
* `<ten>.T` transpose 
* `<ten>.trace()` trace 
* `diag(<ten>)` diagonal of a matrix or a diagonal matrix from a vector
* `eig(<ten>)` or `eigh(<ten>)` diagonalization (use `eigh` for symmetric matrices to ensure that all values stay real)
  
`+=`, `-=`, `*=` and `@=` are also available. 

`<ten>.print(<precision>)` prints the tensor with a given precision (or use default numpy). `print(<ten>)` works too (but prints in the default python format).

`apply` can be used to map any function onto `STen` objects. The result will 
again be transformed to a `STen` object or a tuple of `STen` objects. 
For example, SVD on a matrix `A` can be done as 
```
U,Sig,V = apply(np.linalg.svd, A)
```
A more complicated example using `np.where()` function to zero out eigenvectors which correspond to redundant eigenvalues, and to scale the remaining eigenvector by the corresponding sqrt inverse of the eigenvalues:
```
E, V = eigh(S)
# redundancies
red = apply(lambda x: x < 1e-8, E)
# set small E to 1e3
E = apply(lambda i,w: np.where(i, 1e3, w), red, E)
# zero out redundancies and scale with eigenvalues^-1/2
V = apply(lambda i,x,w: np.where(i, 0.0, x/np.sqrt(w)), red, V, E)
```
