#!/usr/bin/env python3
import sys
import re
from lxml import etree
import numpy as np
from collections import UserList

namespaces={
    'molpro-output': 'http://www.molpro.net/schema/molpro-output',
    'xsd': 'http://www.w3.org/1999/XMLSchema',
    'cml': 'http://www.xml-cml.org/schema',
    'stm': 'http://www.xml-cml.org/schema',
    'xhtml': 'http://www.w3.org/1999/xhtml',
    'xlink': 'http://www.w3.org/1999/xlink'}

ANGSTROM = 1.88972612462577

class MPInfo(object):
    norbs4irreps = []
    molpro_files = dict()
    occvec = []
    closvec = []
    occupation = []
    ncore = []
    charge = 0
    nuclear_energy = 0
    document = 0
    symirreps = []
MP = MPInfo()              

def get_xml_info(where, what):
    """returns an array"""
    return where.xpath('molpro-output:'+what,namespaces=namespaces)

def get_xml_unique(where, what):
    """returns a single element"""
    whats = get_xml_info(where,what)
    if len(whats) != 1:
        raise Exception('something is wrong: there should be just one '+what+' set')
    return whats[0]

def get_xml_last(where, what):
    """returns a single element (last)"""
    whats = get_xml_info(where,what)
    return whats[-1]

def get_xml_variable_vals(document,variable):
    """returns list of values for a given variable"""
    res = []
    for answer in document.xpath('//molpro-output:variables/molpro-output:variable[@name="'+variable+'"]/molpro-output:value',namespaces=namespaces):
        try:
            ans = np.int64(answer.text)
        except ValueError:
            try:
                ans = np.float64(answer.text)
            except ValueError:
                ans = answer.text
        res.append(ans)
    return res

def print_meta_data(molecule):
    metadata = molecule.xpath('stm:metadataList/stm:metadata',namespaces=namespaces)
    print(str(len(metadata))+' metadata items:')
    for md in metadata:
        print(md.get('name') + ' = ' + md.get('content'))

def get_geometry(molecule, print_it=True):
    """Geometry of the molecule"""
    atoms = molecule.xpath('//cml:atomArray/cml:atom',namespaces=namespaces)
    points=np.zeros((len(atoms),3),dtype=np.float64)
    if print_it:
        print(str(len(atoms))+' atoms:')
    ids=np.empty(len((atoms)),dtype='a4')
    element_types=np.empty((len(atoms)),dtype='a4')
    iatom=0
    for atom in atoms:
        if print_it:
            print(atom.get('id')+' '+atom.get('elementType')+' '+atom.get('x3')+' '+atom.get('y3')+' '+atom.get('z3'))
        ids[iatom]=atom.get('id')
        element_types[iatom]=atom.get('elementType')
        points[iatom,:]=[np.float64(atom.get('x3'))*ANGSTROM,np.float64(atom.get('y3'))*ANGSTROM,np.float64(atom.get('z3'))*ANGSTROM]
        iatom+=1
    return points

def get_norbs4irreps(molecule):
    orbitalSet = get_xml_last(molecule,'orbitals')
    norbs = np.zeros(8,np.int64)
    for orbital in get_xml_info(orbitalSet,'orbital'):
        irrep = np.int64(orbital.get('symmetryID'))-1
        norbs[irrep] += 1
    # possible nirreps 1 2 4 8
    if norbs[1:].sum() == 0:
        norbs.resize(1)
    elif norbs[2:].sum() == 0:
        norbs.resize(2)
    elif norbs[4:].sum() == 0:
        norbs.resize(4)
    return norbs

def get_occupation(molecule,norbs):
    occupation = STen(norbs,1)
    occupation.zeros()
    orbitalSet = get_xml_last(molecule,'orbitals')
    iorb = np.zeros(norbs.shape,int)
    for orbital in get_xml_info(orbitalSet,'orbital'):
        irrep = np.int64(orbital.get('symmetryID'))-1
        occupation[irrep][iorb[irrep]] = np.float64(orbital.get('occupation'))
        iorb[irrep] += 1
    return occupation

def get_refdet(molecule,norbs):
    occvec = STen(norbs,1)
    occvec.zeros()
    closvec = STen(norbs,1)
    closvec.zeros()
    orbitalSet = get_xml_last(molecule,'orbitals')
    iorb = np.zeros(norbs.shape,int)
    for orbital in get_xml_info(orbitalSet,'orbital'):
        irrep = np.int64(orbital.get('symmetryID'))-1
        occ = np.float(orbital.get('occupation'))
        if occ > 0:
            occvec[irrep][iorb[irrep]] = 1
            if occ > 1:
                closvec[irrep][iorb[irrep]] = 1
        iorb[irrep] += 1
    return occvec,closvec

def get_refdet_from_occupation(occupation):
    occvec = occupation.copy()
    occvec.zeros()
    closvec = occupation.copy()
    closvec.zeros()
    for irrep in range(len(occupation)):
        for iorb in range(len(occupation[irrep])):
            occ = occupation[irrep][iorb]
            if occ > 0:
                occvec[irrep][iorb] = 1
                if occ > 1:
                    closvec[irrep][iorb] = 1
    return occvec,closvec

def skip_comment_lines(f):
    """skip lines which do not start with a number"""
    not_a_num = True
    while not_a_num:
        #skip comment lines
        pos = f.tell()
        first_char = f.readline().lstrip().lstrip('-')[:1]
        not_a_num = not first_char.isnumeric()
    f.seek(pos)

def skip_to_next_after_string(f,string):
    """skip lines which do not start with string (including the first occurence)"""
    found= False
    while not found:
        found = (f.readline().lstrip()[:len(string)] == string)

def read_numbers_in_line(f):
    line = f.readline()
    numbs = [num for num in re.split(' |,|\n',line) if num]
    return np.array(numbs,np.float64)

def match_irreps(irreps1,irreps2):
    """match two lists of irreps 
       on return irrep_perm such that irreps1[irrep_perm] == irreps2
    """
    irreps_1 = [irrep.upper() for irrep in irreps1]
    irreps_2 = [irrep.upper() for irrep in irreps2]
    irrep_perm = []
    check_symperm = [0]*len(irreps_1)
    for irrep in irreps_1:
        try:
            irrep_perm.append(irreps_2.index(irrep))
            if check_symperm[irrep_perm[-1]] != 0:
                print(irrep,irreps_1,irreps_2)
                raise Exception("Repeated irrep id!")
            else:
                check_symperm[irrep_perm[-1]] = 1
        except ValueError:
            raise Exception("Irrep",irrep,"not found in ",irreps_2)
    return irrep_perm

def read_matrix(mat,f):
    """read preallocated matrix mat from file f"""
    leftdim = mat.shape[0]
    rightdim = mat.shape[1]
    prestart = 0
    for irow in range(leftdim):
        start = prestart
        prestart = 0
        while start < rightdim:
            numbs = read_numbers_in_line(f)
            end = start + len(numbs)
            if end <= rightdim:
                mat[irow,start:end] = numbs
            else:
                rest = end - rightdim
                incl = len(numbs) - rest
                mat[irow,start:] = numbs[:incl]
                mat[irow+1,0:rest] = numbs[incl:]
                prestart = rest
            start = end
    return mat

def read_matrix_from_file(ascii_file):
    """read a simple matrix from ascii file
       the dimensions have to be provided in the first line of file
       in the form `# MATRIX 4 3` for a 4x3 matrix (first index is the row index)
    """
    with open(ascii_file,'r') as f:
        firstline = [str for str in f.readline().upper().split() if str]
        idim = 0
        leftdim = 0
        rightdim = 0
        if firstline[0] == "#" and firstline[1] == "MATRIX":
            idim = 2
        elif firstline[0] == "#MATRIX" or firstline[0] == "#":
            idim = 1
        else:
            raise Exception("First line should have format `# MATRIX M N`")
        try:
            leftdim = int(firstline[idim])
            rightdim = int(firstline[idim+1])
        except ValueError:
            raise Exception("First line should have format `# MATRIX M N`")
        mat = np.empty([leftdim,rightdim])
        skip_comment_lines(f)
        # read the matrix
        mat = read_matrix(mat,f)
    return mat

class STen(UserList):
    def __init__(self, init_array=None, ndim=2):
        self.data = []
        if init_array is None:
            # use info from MP
            init_array = MP.norbs4irreps
        read_from_file = None
        if isinstance(init_array,str):
            # from molpro files
            read_from_file = MP.molpro_files[init_array]
            init_array = MP.norbs4irreps
        if isinstance(init_array, np.ndarray):
            # from number of orbitals in each irrep
            for norb4ir in init_array:
                sizes = tuple([norb4ir]*ndim)
                self.append(np.empty(sizes))
        elif isinstance(init_array, map):
            # create a list first...
            super().__init__(list(init_array))
        else:
            super().__init__(init_array)
        if read_from_file:
            self.import_from_file(read_from_file)

    def zeros(self):
        self.data = [np.zeros(x.shape,np.float64) for x in self]
        return self
    def ones(self):
        self.data = [np.ones(x.shape,np.float64) for x in self]
        return self
    def identity(self):
        self.data = [np.identity(x.shape[0],np.float64) for x in self]
        return self

    def nirreps(self):
        return len(self)
    
    def nbasis(self):
        return sum([ten.shape[0] for ten in self])
        
    def import_from_file(self,ascii_file):
        """import matrix or vector from ascii file"""
        with open(ascii_file,'r') as f:
            skip_comment_lines(f)
            # read the matrices
            for ten in self:
                nelem = ten.shape[0]
                if ten.ndim == 1:
                    # read the vectors
                    start = 0
                    while start < nelem:
                        numbs = read_numbers_in_line(f)
                        end = start + len(numbs)
                        ten[start:end] = numbs 
                        start = end        
                elif ten.ndim == 2:            
                    for irow in range(nelem):
                        start = 0
                        while start < nelem:
                            numbs = read_numbers_in_line(f)
                            end = start + len(numbs)
                            ten[irow,start:end] = numbs
                            start = end
                else:
                    raise Exception("Can read only vectors or matrices!")
    def export_to_file(self,ascii_file, num_in_line=5):
        """export matrix or vector to ascii file
           write num_in_line numbers per line"""
        def write_vec(f, vec):
            nelem = len(vec)
            start = 0
            nonlocal num_in_line
            while start < nelem:
                end = start + num_in_line
                if end > nelem:
                    end = nelem
                print(", ".join(['{: .11E}'.format(el) for el in vec[start:end]]), ",", file=f, sep='')
                start = end

        with open(ascii_file, 'w') as f:
            for ten in self:
                if ten.ndim == 1:
                    # write the vectors
                    write_vec(f, ten)
                elif ten.ndim == 2:
                    nelem = ten.shape[0]
                    for irow in range(nelem):
                        write_vec(f, ten[irow,:])
    def export(self, what, num_in_line=5):
        """export using MP.molpro_files"""
        self.export_to_file(MP.molpro_files[what],num_in_line)

    def get_nosym(self):
        """generate numpy array without point-group symmetry (fill up with zeros)"""
        res = np.zeros(self[0].ndim*[self.nbasis()])
        offs = 0
        for ten in self:	
            nelem = ten.shape[0]
            offs1 = offs+nelem
            if ten.ndim == 1:
                res[offs:offs1] = ten
            elif ten.ndim == 2:
                res[offs:offs1,offs:offs1] = ten
            elif ten.ndim == 3:
                res[offs:offs1,offs:offs1,offs:offs1] = ten
            elif ten.ndim == 4:
                res[offs:offs1,offs:offs1,offs:offs1,offs:offs1] = ten
            elif ten.ndim > 4:
                raise Exception("nosym is implemented upto 4 dim tensors only!")
            offs = offs1    	
        return res

    def set_nosym(self,arr,tol=1.e-6):
        """set all elements from a nosym numpy array. 
        Stop if there are large elements ouside the symmetries"""
        if arr.ndim != self[0].ndim:
            raise Exception("Mismatch in number of dimensions in set_nosym!")
        nbas = arr.shape[0]
        if nbas != self.nbasis():
            print(nbas,self.nbasis())
            raise Exception("Mismatch in the total size of basis in set_nosym!")
        if arr.shape.count(nbas) != arr.ndim:
            raise Exception("Only arrays with same number of orbitals in each direction are possible in set_nosym!")
        offs = 0
        for ten in self:	
            nelem = ten.shape[0]
            offs1 = offs+nelem
            if ten.ndim == 1:
                ten[:] = arr[offs:offs1]
            elif ten.ndim == 2:
                ten[:,:] = arr[offs:offs1,offs:offs1] 
                print(ten)
            elif ten.ndim == 3:
                ten[:,:,:] = arr[offs:offs1,offs:offs1,offs:offs1]
            elif ten.ndim == 4:
                ten[:,:,:,:] = arr[offs:offs1,offs:offs1,offs:offs1,offs:offs1]
            elif ten.ndim > 4:
                raise Exception("nosym is implemented upto 4 dim tensors only!")
            offs = offs1    	
        # check
        if np.amax(np.abs(self.get_nosym()-arr)) > tol:
            print("Largest element outside symmetries:",np.amax(np.abs(self.get_nosym()-arr)))
            raise Exception("Elements outside STen symmetries above tol threshold!")

    def __add__(self, other):
        try:            	
            iterator = iter(other)
        except TypeError:
            return STen(map(lambda x: x + other,self))
        else:
            return STen(map(lambda x,y: x + y,self,other))
    __radd__ = __add__
    def __sub__(self, other):
        try:
            iterator = iter(other)
        except TypeError:
            return STen(map(lambda x: x - other,self))
        else:
            return STen(map(lambda x,y: x - y,self,other))
    def __rsub__(self, other):
        return -(self - other)
    def __neg__(self):
        return STen(map(lambda x: -x,self))
    def __mul__(self, other):
        try:
            iterator = iter(other)
        except TypeError:
            return STen(map(lambda x: x * other,self))
        else:
            return STen(map(lambda x,y: x * y,self,other))
    __rmul__ = __mul__
    def __matmul__(self, other):
        return STen(map(lambda x,y: np.matmul(x,y),self,other))
    matmul = __matmul__
    def dot(self, other):
        try:
            iterator = iter(other)
        except TypeError:
            return STen(map(lambda x: x.dot(other),self))
        else:
            return STen(map(lambda x,y: x.dot(y),self,other))
    def transpose(self):
        return STen(map(lambda x: x.transpose(),self))

    # descriptor for transpose
    class Transpose:
        def __get__(self, obj, objtype=None):
            return obj.transpose()
    T = Transpose()

    def __iadd__(self, other):
        self.data = list(self.__add__(other))
        return self
    def __isub__(self, other):
        self.data = list(self.__sub__(other))
        return self
    def __imul__(self, other):
        self.data = list(self.__mul__(other))
        return self
    def __imatmul__(self, other):
        self.data = list(self.__matmul__(other))
        return self

    def trace(self):
        return sum([mat.trace() for mat in self])

    def print(self,precision=None):
        irrep = 0
        for ten in self:
            print("Irrep:",irrep)
            irrep += 1
            if precision is None:
                print(ten)
            else:
                with np.printoptions(precision=precision, suppress=True):
                    print(ten)


def apply(func,*args):
    """apply function to arguments and return appropriate sym.arrays, tuples of sym.arrays or a list"""
    if len(args) == 1:
        res = list(map(func,args[0]))
    elif len(args) == 2:
        res = list(map(func,args[0],args[1]))
    elif len(args) == 3:
        res = list(map(func,args[0],args[1],args[2]))
    elif len(args) == 4:
        res = list(map(func,args[0],args[1],args[2],args[3]))
    elif len(args) == 5:
        res = list(map(func,args[0],args[1],args[2],args[3],args[4]))
    elif len(args) == 6:
        res = list(map(func,args[0],args[1],args[2],args[3],args[4],args[5]))
    elif len(args) == 7:
        res = list(map(func,args[0],args[1],args[2],args[3],args[4],args[5],args[6]))
    elif len(args) == 8:
        res = list(map(func,args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]))
    elif len(args) == 9:
        res = list(map(func,args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8]))
    else:
        raise Exception("Only up to 9 arguments are possible in apply...")
    if type(res[0]) is tuple:
        retlist = []
        for i in range(len(res[0])):
            retlist.append(STen([tup[i] for tup in res]))
        return tuple(retlist)
    elif isinstance(res[0],np.ndarray):
        return STen(res)
    else:
        return res

def eig(amat):
    """diagonalize matrix, return eignvalues,eigenvectors"""
    eigval = STen([])
    eigvec = STen([])
    for a in amat:
        e,v = np.linalg.eig(a)
        indx = e.argsort()
        eigval.append(e[indx])
        eigvec.append(v[:,indx])
    return eigval,eigvec

def eigh(amat):
    """diagonalize hermitian matrix, return eignvalues,eigenvectors"""
    eigval = STen([])
    eigvec = STen([])
    for a in amat:
        e,v = np.linalg.eigh(a)
        indx = e.argsort()
        eigval.append(e[indx])
        eigvec.append(v[:,indx])
    return eigval,eigvec

def diag(ten):
    """return diagonal for matrix or a diagonal matrix for vector"""
    return STen(map(np.diag, ten))

def inv(amat):
    """return inverse of a matrix"""
    return apply(lambda x: np.linalg.inv(x),amat)
    
def pinv(amat, tol=1.e-12):
    """calc pseudoinverse, neglecting singularities below tol"""
    return apply(lambda x: np.linalg.pinv(x,tol),amat)
    # doing it by hand:
    # umat, sigma, vmat = apply(np.linalg.svd, amat)
    # sigma = apply(lambda x: np.where(x < tol, 0.0, 1/x), sigma)
    # return (umat * sigma) @ vmat

def export_data_parser(edfile):
    """Parser for exportdata file included in molpro input"""
    vardict = dict()
    with open(edfile,'r') as f:
        done = False
        while not done:
            line = f.readline().lstrip()
            if not line:
                continue
            elif line[0] == "$":
                varval = [var for var in re.split(' |=|\n',line[1:]) if var] 
                if len(varval) != 2:
                    raise Exception("Could not parse the variable pair "+line)
                vardict[varval[0]] = varval[1].strip("'\"")
            elif line[0] =="!":
                # skip comment
                continue
            else:
                # something else, finish
                done = True
    return vardict

def setup_mpinfo(exportdata_file = 'exportdata'):
    """Handle options, setup all the data in MP"""
    MP.molpro_files = export_data_parser(exportdata_file)
    # print(molpro_files)

    tree = etree.parse(MP.molpro_files['XMLFILE'])
    MP.document = tree.getroot()
    molecule = get_xml_unique(MP.document,'molecule')
    print('Molecule ' + molecule.get('id'))
    # PrintMetaData(molecule)
    # GetGeometry(molecule)
    # print("Basis:",get_xml_variable_vals(document,"_BASIS"))
    symmetry = get_xml_unique(molecule,'symmetry')
    MP.symirreps = [sym.get('id') for sym in get_xml_info(symmetry,'irreducibleRepresentation')]
    # print ("nirreps = ",len(MP.symirreps))

    MP.norbs4irreps = get_norbs4irreps(molecule) 
    if len(MP.norbs4irreps) != len(MP.symirreps):
        print(MP.norbs4irreps)
        print(len(MP.symirreps))
        raise Exception("inconsistency in number of irreps!")
    MP.occupation = get_occupation(molecule,MP.norbs4irreps)
    #MP.occvec,MP.closvec = get_refdet(molecule,MP.norbs4irreps)
    MP.occvec,MP.closvec = get_refdet_from_occupation(MP.occupation)
    # MP.occvec.print()
    # MP.closvec.print()
    # print('End of molecule '+molecule.get('id'))
    MP.ncore = get_xml_variable_vals(MP.document,"_CORE")
    if len(MP.ncore) == 0:
        MP.ncore = get_xml_variable_vals(MP.document,"!DEFCORE")
    MP.ncore = MP.ncore[:len(MP.norbs4irreps)]
    # print("Core:",MP.ncore)
    charge = get_xml_variable_vals(MP.document,"CHARGE")
    if len(charge) > 0:
        MP.charge = charge[0]
    nuclear_energy = get_xml_variable_vals(MP.document,"_ENUC")
    if len(nuclear_energy) > 0:
        MP.nuclear_energy = nuclear_energy[0]

def check_args(exportdata_file):
    exportdata_file = 'exportdata'
    args_loop = iter(sys.argv[1:])
    for arg in args_loop:
        if arg.startswith("-"):
            if arg.startswith("-i"):
                # input file
                arg = next(args_loop)
                exportdata_file = arg
            elif arg.startswith("-h"):
                # print help
                print("Usage: mp.py <exportdata file>")
            else:
                print("option "+arg+" not known")
        else:
            exportdata_file = arg

def main(exportdata_file):
    setup_mpinfo(exportdata_file)
    orbitals = STen('ORBFILE')
    # orbitals = STen(MP.norbs4irreps)
    # orbitals.import_from_file(MP.molpro_files['ORBFILE'])
    print("ORBITALS")
    orbitals.print()

    overlap = STen('SFILE')
    # overlap.print()
    print("MO overlap")
    (orbitals.T.matmul(overlap).matmul(orbitals)).print(8)

    umat,sigmavec,vmat = apply(np.linalg.svd,overlap)
    sigmavec.print()

    # occupied orbitals:
    C_occ = apply(lambda o,c: np.where(o==0, 0.0, c), MP.occvec, orbitals)
    # or
    C_occ = orbitals * MP.occvec
    (C_occ.T.matmul(overlap).matmul(C_occ)).print(8)

if __name__ == '__main__':
    exportdata_file = 'exportdata'
    check_args(exportdata_file)
    main(exportdata_file)
