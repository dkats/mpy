#!/usr/bin/env python3
from mp import *

# the main program
setup_mpinfo()
#get a variable from xml file
molname = get_xml_variable_vals(MP.document,"MOL")
if len(molname) > 0:
    print(molname[0])

C = STen('ORBFILE')
S = STen('SFILE')
# occupied orbitals
Co = C * MP.occvec
SCo = S @ Co
#PAO coeffs
PAO = STen().identity() - Co @ SCo.T
#PAO overlap
SPAO = PAO.T @ S @ PAO

E, V = eigh(SPAO)
# redundancies
red = apply(lambda x: x < 1e-8, E)
# set small E to 1e3
E = apply(lambda i,w: np.where(i, 1e3, w), red, E)
# zero out redundancies and scale with eigenvalues^-1/2 
V = apply(lambda i,x,w: np.where(i, 0.0, x/np.sqrt(w)), red, V, E)
# orthogonal PAOs
oPAO = PAO @ V
# add occupied orbitals
OPAO = oPAO + Co
(OPAO.T @ S @ OPAO).print(5)
OPAO.export('OPAOFILE')

nocc = [int(sum(o)) for o in MP.occvec]
nclosed = [int(sum(c)) for c in MP.closvec]
nsym = len(nocc)
nocc[nsym:8] = [0]*(8-nsym) 
nclosed[nsym:8] = [0]*(8-nsym)

with open("pyvars",'w') as f:
    #print("set,core=[",",".join([str(int(x)) for x in ncore]),"]", sep='', file=f)
    print("NOCC=[",",".join([str(int(x)) for x in nocc]),"]", sep='', file=f)
    print("NCLOS=[",",".join([str(int(x)) for x in nclosed]),"]", sep='', file=f)
